using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public static int EnemiesAlive = 0;
    //public Transform enemyPrefab;
    
    
    public GameObject[] enemies;
    public List<Waves> waves = new List<Waves>();
    public int basicCount = 0;
    public int toughCount = 0;
    public int fastCount = 0;
    public int totalCount = 0;
    public int currentWave = 0;
    public static int aliveCount = 0;
    
    public Transform spawnPoint;
    public float timeBetweenWaves = 5f;
    private float countdown = 2f;
    public Text waveCountDownText;
    private int waveIndex;

    void Update()
    {
        if (aliveCount > 0)
        {
            return;
        }
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;
        }
        if (GameManager.gameIsOver)
        {
            this.enabled = false;
        }
        countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        waveCountDownText.text = string.Format("{0:00}", countdown);
    }
    IEnumerator SpawnWave()
    {

        //waveIndex++;
        PlayerStats.Rounds++;
        Debug.Log("Ronda mas!");
        for (int i = 0; i < waves[currentWave].totalEnemies; i++)
        {
            if (basicCount >= 5 /*&& totalCount <= waves.Count*/)
            {
                SpawnTough();
                totalCount++;
                toughCount++;
            }
            else if (basicCount >= 0)
            {
                SpawnEnemy();
                totalCount++;
                yield return new WaitForSeconds(0.75f);
            }
            else if (toughCount >= 3)
            {
                SpawnFast();
                totalCount++;
                Debug.Log("Spawnea fast");
            }  
            //if (currentWave > waves.Count)
            {
                //Debug.Log("Fin de nivel");
            }
        }
        currentWave++;
    }
    void SpawnEnemy()
    {
        Instantiate(enemies[0], spawnPoint.position, spawnPoint.rotation);
        //EnemiesAlive++;
        basicCount++;
        aliveCount++;
        
    }

    void SpawnTough()
    {
        Instantiate(enemies[1], spawnPoint.position, spawnPoint.rotation);
       /*Enemy yc = GameObject.Find("Tough(Clone)").GetComponent<Enemy>();
       yc.speed = 100f;*/
        basicCount = 0;
        aliveCount++;
        toughCount++;
    }
    void SpawnFast()
    {
        Instantiate(enemies[2], spawnPoint.position, spawnPoint.rotation);
        aliveCount++;
        fastCount++;
    } 
}