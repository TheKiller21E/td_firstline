﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public static int Money;
    public int startMoney = 1000;
    public int maxHealth = 100;
    public static int currentHealth;
    public int currenthealth;
    public static int Rounds;

    public HealthBar healthBar;

    private void Start()
    {
        Money = startMoney;
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

        Rounds = 0;
    }
    public void Update()
    {
        currenthealth = currentHealth;
        GameObject ac = GameObject.Find("BarradeVida");
        ac.GetComponent<HealthBar>().SetHealth(currenthealth);
    }

}
