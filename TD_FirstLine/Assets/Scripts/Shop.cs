﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standardTurret;
    public TurretBlueprint sniperTurret;
    BuildManager buildManager;
    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    public void SelectStandardTurret()
    {
        Debug.Log("Comprada");
        buildManager.SelectTurretToBuild(standardTurret);
    }
    public void SelectSniperTurret()
    {
        Debug.Log("Comprada otra");
        buildManager.SelectTurretToBuild(sniperTurret);
    }
}
